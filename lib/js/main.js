var started_poll = false;
var storageData = [];
var parties_scores = {};
var subjects_titles = [];
var party_titles = [];
let selected_subjects = [];
let selected_parties = [];
var topParties = [];

var data = {
  array: null,
  question: 0,
  value: null
};

function setupPartyScores() {
  subjects[0].parties.forEach(party => {
    parties_scores[party.name] = 0;

  })
}


// This method starts the stemwijzer
function start() {
  display();
  showData();
  setupPartyScores();
}

// When pressed on a button for each argument
function vote(value) {
  data.value = value;
  data.question++;
  //Checks if the stemwijzer is finished
  if (data.question > subjects.length - 1) {
    data.question = storageData.length;
    checkData();
    displayResult();
    //If it isn't finished go to the next question and display the data
  } else {
    storeData(subjects[data.question], data.question, data.value);
    showData();
  }
}

//Goes to the previous question
function goBack() {
  //Remove last element from storage
  storageData.pop();
  data.question--;
  //Check if the question goes to 0 and redirects to the home page
  if (data.question < 0) {
    displayMenu();
    data.question = 0;
    //Continues with the stemwijzer to the next question
  } else {
    showData();
  }

}

//Displays the current question you're on
function showData(question) {
  animate();
  question = data.question;
  statement_title.innerHTML = question + 1 + ". " + subjects[question].title;
  statement.innerHTML = subjects[question].statement;
}





/**
 * This function keeps track of the answers you have given for each question and sends the data to the array
 * @param  {array} array the array that is given the the full list of parties
 * @param {number} question the question number
 * @param {number} value the answer you have selected 
 * @return {null}  
 */

function storeData(array, question, value) {
  // Save the data and send it to the array of the choice you have clicked
  storageData.push(answer = {
    array: array,
    question: question,
    value: value,
    multiplier: 1
  });

}

/*
In this checkData function, it is adding all the points of the choices you made so far.
It also checks all the parties you have selected to have a extra value so the multiplier gets doubled
it filters all the answers and check if the answer matches and adds point to the given multiplier value
*/
function checkData() {
  storageData.forEach(question => {
    let answer = question.value;
    var multiplier = question.multiplier;

    //Checks the parties that been selected and gives a extra value
    party_titles.forEach(party => {
      if (party.isSelected) {
        multiplier = 2;
      } else {
        multiplier = 1;
      }
    })
    console.log(multiplier);
    //Return parties position value
    let filterArray = question.array.parties.filter(p => {
      if (p.position === answer) {
        parties_scores[p.name] += multiplier;
      }
      return p.position === answer;
    })


  })
  // Check all the points in the array with parties and show top 3
  displayParties(3);

}

// Checks the subjects that have been selected 
function subjectWeight() {
  subjects_titles.forEach(subject => {
    if (subject.checked) {
      selected_subjects.push(subject.name);
      subject.selected = true;

    }
    subject.checked = false;
  })
  // Generate the party list on screen
  loadPartyNames();

}


/*
Here we check which checkboxes has been selected from the generated parties list on the screen
whenever its selected its being send to the array and also redirects it to the function, 
that checks how many are selected since you need to have atleast 3 selected to continue;
*/
function partyWeight() {
  var count = 0;
  //Check which parties have been selected
  for (var i = 0; i < party_titles.length; i++) {
    if (party_titles[i].type == "checkbox" && party_titles[i].checked == true) {
      selected_parties.push(party_titles[i].name);
      party_titles[i].isSelected = true;
      count++;
    }
    party_titles[i].checked = false;

    console.log(party_titles[i].isSelected);
  }
  // checking if 3 or more is selected
  checkPartyCount(count);
}

function checkPartyCount(c) {
  if (c >= 3) {
    loadEndResult(topParties);
  } else {

  }
}




/**
 * It sorts them all out with their score value and makes sure there isn't any duplicate in the list.
 * @param  {Number} numToDisplay amount of parties to be shown
 * @return {null}  
 */

function displayParties(numToDisplay) {
  //Checks the number of the argument
  for (var i = 0; i < numToDisplay; i++) {
    var largestValue = 0;
    var largestParty;
    // Assigns each parties point in a ranking
    for (let [key, value] of Object.entries(parties_scores)) {
      if (value > largestValue && !topParties.includes(key)) {
        largestValue = value;
        largestParty = key;
      }
    }
    // Sends the top x given argument to the array so it can be shown at the result screen
    topParties.push(largestParty);

  }

}





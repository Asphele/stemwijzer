window.onload = init();
var start_button,
  statement_title,
  statement,
  agree_button,
  disagree_button,
  none_button,
  logo,
  partiesBar,
  back;

function init() {
  start_button = document.querySelector(".start");
  statement_title = document.querySelector(".statement_title");
  statement = document.querySelector(".statement_text");
  partiesBar = document.querySelector(".parties");
  agree_button = document.querySelector(".agree");
  disagree_button = document.querySelector(".disagree");
  none_button = document.querySelector(".none");
  skip_button = document.querySelector(".skip_button");
  back = document.querySelector(".back-arrow");


  agree_button.onclick = function () {
    vote("pro");
  };
  disagree_button.onclick = function () {
    vote("contra");
  };

  none_button.onclick = function () {
    vote("none");
  };
  skip_button.onclick = function () {
    vote("null");
  };

  // for (i = 0; i < parties.length; i++) {
  //   parties[i].selectedParties = false;
  //   parties[i].selectedSubjects = false;
  // }
  // console.log(parties);
}

function display() {
  start_button.classList.add("fadeAway");
  document.querySelector(".intro").style.display = "none";
  document.querySelector(".module").style.display = "none";
  document.querySelector(".module2").style.display = "block";
  partiesBar.style.display = "none";
}

function displayMenu() {
  start_button.classList.remove("fadeAway");
  document.querySelector(".intro").style.display = "block";
  document.querySelector(".module").style.display = "block";
  document.querySelector(".module2").style.display = "none";
  partiesBar.style.display = "block";
}

function statementDisplay() {
  document.querySelector(".module2").style.display = "none";
  document.querySelector(".module3").style.display = "block";

  loadParties();
}

function goBackQuestions(input) {
  document.querySelector(".module3").style.display = "none";
  document.querySelector(".module2").style.display = "block";

  if (input == 0) {
    document.querySelector(".module4").style.display = "none";
    document.querySelector(".module2").style.display = "block";
  }
}

function animate(trigger) {
  let element = document.getElementById("animate");
  element.classList.add("next");
  element.style.display = "none";
  setTimeout(() => {
    element.style.display = "block";
  }, 100);
}

function displayResult() {
  document.querySelector(".module2").style.display = "none";
  document.querySelector(".module4").style.display = "block";

  loadSubjects();

}

function loadParties() {
  document.querySelector(".opinion_title").innerHTML =
    statement_title.innerHTML;

  let agree = document.querySelector(".statement_agree");
  let disagree = document.querySelector(".statement_disagree");
  let none = document.querySelector(".statement_none");

  let holder = document.querySelector(".holder");

  let index = data.question;

  let list = [agree, disagree, none];

  console.log(holder.children.item());
}

function loadSubjects() {
  document.querySelector(".all_buttons").style.display = "none";
  document.querySelector('.parties_titles').style.display = "none";
  let subjectsEl = document.querySelector('.subjects');
  let subbutton = document.querySelector(".subbutt");
  subbutton.addEventListener("click", subjectWeight);
  subjectsEl.innerHTML = "";

  for (i = 0; i < subjects.length - 1; i++) {
    var checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.name = subjects[i].title;
    checkbox.value = subjects[i].title;
    checkbox.id = i + 1;
    checkbox.selected = false;

    var label = document.createElement("label");
    var labelText = document.createTextNode(subjects[i].title);
    label.append(labelText);

    var br = document.createElement("br");

    subjectsEl.append(label);
    subjectsEl.append(checkbox);
    subjects_titles.push(checkbox);

  }




}

function loadPartyNames() {
  document.querySelector('.subjects').innerHTML = "";
  document.querySelector(".all_buttons").style.display = "inline-block";
  document.querySelector('.parties_titles').style.display = "inline-block";

  let subjectsEl = document.querySelector('.parties_titles');
  let title = document.querySelector(".end_title");
  let button = document.querySelector(".patbutt");

  button.onclick = partyWeight();

  subjectsEl.innerHTML = "";
  title.innerHTML = "Kies welke partijen je belangrijk vond";
  console.log(subjects)

  for (i = 0; i < parties.length; i++) {
    var checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.name = parties[i].name;
    checkbox.value = parties[i].name;
    checkbox.id = "id";
    checkbox.selected = false;

    var label = document.createElement("label");
    var labelText = document.createTextNode(parties[i].name);
    label.append(labelText);

    var br = document.createElement("br");

    subjectsEl.append(label);
    subjectsEl.append(checkbox);
    party_titles.push(checkbox);

  }


}

function loadEndResult(parties) {
  document.querySelector(".module4").style.display = "none";
  document.querySelector(".module5").style.display = "block";
  let holder = document.querySelector(".top_parties");


  console.log(parties);

  for (var i = 0; i < 3; i++) {
    var p = document.createElement("div");
    p.innerHTML = `${i + 1}  ${parties[i]}`;

    holder.append(p);
  }
}

function selectAllParties() {
  let parties = party_titles;

  parties.forEach(p => {
    p.checked = true;
  })
}

function selectSecParties() {
  for (i = 12; i < party_titles.length; i++) {
    party_titles[i].checked = true;

  }
}

function selectZitParties() {
  for (i = 0; i < 12; i++) {
    party_titles[i].checked = true;

  }
}

function unselectAllParties() {
  let parties = party_titles;

  parties.forEach(p => {
    p.checked = false;
  })
}